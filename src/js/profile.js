// ============================================
// HARTY911 : load/save profile in localstorage

const PROFILE_KEY = 'covid-profile'
const PROFILE_FIELDS = ['firstname', 'lastname', 'birthday', 'placeofbirth', 'address', 'city', 'zipcode']

export function loadProfile () {
  let storage = localStorage.getItem(PROFILE_KEY)

  if (window.location.hash) {
    try {
      storage = atob(window.location.hash.substr(1))
    } catch (e) {
      console.error('Invalid Base64 profile')
    }
  }
  return storage ? JSON.parse(storage) : undefined
}

export function loadFormValues (formInputs) {
  const oldProfile = localStorage.getItem('codiv--profile')
  if (oldProfile) {
    console.log('clear previous profile')
    localStorage.clear()
    localStorage.setItem(PROFILE_KEY, oldProfile)
  }
  // HARTY911 set default value
  const profile = loadProfile() || {}
  formInputs.forEach((input) => {
    if (input.name && profile[input.name] && PROFILE_FIELDS.includes(input.name)) {
      input.value = profile[input.name]
    }
  })
}

export function saveFormValues (formInputs) {
  // HARTY911 set default value
  const profile = {}
  formInputs.forEach((input) => {
    if (PROFILE_FIELDS.includes(input.name)) {
      profile[input.name] = input.value
    }
  })
  const strProfile = JSON.stringify(profile)
  localStorage.setItem(PROFILE_KEY, strProfile)

  // append B64 profile hash to URL
  document.location.hash = '#' + btoa(strProfile)
}

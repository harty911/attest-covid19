import { library, dom } from '@fortawesome/fontawesome-svg-core'
import { faFilePdf, faTrash, faWheelchair, faShoppingCart, faBriefcaseMedical, faWalking, faBuilding, faGraduationCap, faArrowLeft, faSearch, faEllipsisH } from '@fortawesome/free-solid-svg-icons'

library.add(faWalking, faBriefcaseMedical, faWheelchair, faShoppingCart, faBuilding, faGraduationCap, faEllipsisH)
library.add(faArrowLeft, faSearch)
library.add(faFilePdf)
library.add(faTrash)

dom.watch()

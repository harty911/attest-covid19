import 'bootstrap/dist/css/bootstrap.min.css'
import '../css/emergency.css'
import './icons'

import pdfBase from '../certificate.pdf'
import { generatePdf } from './pdf-util'
import { $$, createElement } from './dom-utils'
import { loadProfile } from './profile'

document.addEventListener('DOMContentLoaded', onLoad, false)

function onLoad () {
  $$('.bar-btn').forEach((el) => {
    el.addEventListener('click', () => {
      document.location.reload()
    })
  })
  $$('.back-btn').forEach((el) => {
    // document.body.requestFullscreen()
  })

  $$('.shortcut-btn').forEach((el) => {
    const reason = el.dataset.reason
    const minAgo = parseInt(el.dataset.retro)

    // N minutes ago ;-)
    const dt = new Date(Date.now() - minAgo * 60000)
    const creationDate = dt.toLocaleDateString('fr-CA')
    const creationHour = dt.toLocaleTimeString('fr-FR', { hour: '2-digit', minute: '2-digit' })
      .replace(':', '-')
    const filename = `attestation-${creationDate}_${creationHour}.pdf`

    el.querySelector('.filename').innerHTML = filename
    el.querySelector('.comment').innerHTML =
      `${dt.toLocaleDateString('fr-FR')} ${dt.toLocaleTimeString('fr-FR')} ${reason}`

    el.addEventListener('click', async () => await generateEmergency(reason, dt, filename))
  })
}

export async function generateEmergency (reasons, dt, filename) {
  const profile = loadProfile()
  if (!profile) return 'No profile !'

  profile.datesortie = dt.toLocaleDateString('fr-FR')
  profile.heuresortie = dt.toLocaleTimeString('fr-FR', { hour: '2-digit', minute: '2-digit' })

  const pdfBlob = await generatePdf(profile, reasons, pdfBase, dt)
  console.log('blob', pdfBlob)
  openBlob(pdfBlob, filename)
}

export function openBlob (blob) {
  const link = createElement('a')
  const url = URL.createObjectURL(blob)
  link.href = url
  document.body.appendChild(link)
  link.click()
}

# Générateur de certificat de déplacement

## Développer

### Installer le projet

```console
git clone https://gitlab.com/harty911/attest-covid19.git
cd attest-covid19
npm install
npm start
```

## Production

### Build dist

```console
npm run build
```

## Crédits

Ce projet a été réalisé à partir d'un fork du dépôt [attestation-deplacement-derogatoire-q4-2020](https://github.com/lab-mi/attestation-deplacement-derogatoire-q4-2020)

Les projets open source suivants ont été utilisés pour le développement de ce
service :

- [PDF-LIB](https://pdf-lib.js.org/)
- [qrcode](https://github.com/soldair/node-qrcode)
- [Bootstrap](https://getbootstrap.com/)
- [Font Awesome](https://fontawesome.com/license)
